package core;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.api.objects.Message;
import utils.sql.DatabaseManagement;

import java.io.IOException;


public class MessageSender {
    public static DefaultAbsSender sender = null;

    public static void sendMessage(Long ChatId, String Message) {
        SendMessage msg = new SendMessage();
        msg.setChatId(ChatId);
        msg.setText(Message);
        DefaultBotOptions options = new DefaultBotOptions();
        sender = new DefaultAbsSender(options) {
            @Override
            public String getBotToken() {
                return Haruka.HarukaBot.getBotToken();
            }
        };
        try {
            Message sentMessage = sender.execute(msg);
            Haruka.messageOutput(sentMessage);
            DatabaseManagement.addMessage(sentMessage);
        } catch (TelegramApiException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendReply(Long ChatId, String Message, int ReplyTo) {
        SendMessage msg = new SendMessage();
        msg.setChatId(ChatId);
        msg.setText(Message);
        msg.setReplyToMessageId(ReplyTo);
        DefaultBotOptions options = new DefaultBotOptions();
        sender = new DefaultAbsSender(options) {
            @Override
            public String getBotToken() {
                return Haruka.HarukaBot.getBotToken();
            }
        };
        try {
            Message sentMessage = sender.execute(msg);
            Haruka.messageOutput(sentMessage);
            DatabaseManagement.addMessage(sentMessage);
        } catch (TelegramApiException | IOException e) {
            e.printStackTrace();
        }
    }
}