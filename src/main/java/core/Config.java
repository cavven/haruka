package core;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Config {

    private String botToken, logSqlServer, logSqlUsername, logSqlPass, logSqlDatabaseName, logSqlTableName,
            chatsToRepeat, repeaterChatId, chatsToRepostFrom, reposterControlChatId, reposterFinalDestId, citationsSqlServer,
            citationsSqlUsername, citationsSqlPass, citationsSqlDatabaseName, citationsSqlTableName, botUserNames, buildReportChat;
    private final ArrayList<String> chatsToRepeatList = new ArrayList<>();
    private final ArrayList<String> chatsToRepostFromList = new ArrayList<>();
    private final ArrayList<String> botUserNamesList = new ArrayList<>();
    private boolean repeater, reposter;

    Config(){
        init();
    }

    private void init() {
        readConfig();
    }

    private void readConfig() {
        checkConfigExistence();
        JSONParser parser = new JSONParser();
        JSONObject config;
        try {
            config = (JSONObject) parser.parse(new FileReader("config/haru.json"));
            botToken = (String) config.get("token");

            logSqlServer = (String) config.get("log_sqlserver");
            logSqlUsername = (String) config.get("log_sqlusername");
            logSqlPass = (String) config.get("log_sqlpass");
            logSqlDatabaseName = (String) config.get("log_sqldatabasename");
            logSqlTableName = (String) config.get("log_sqltablename");

            citationsSqlServer = (String) config.get("citations_sqlserver");
            citationsSqlUsername = (String) config.get("citations_sqlusername");
            citationsSqlPass = (String) config.get("citations_sqlpass");
            citationsSqlDatabaseName = (String) config.get("citations_sqldatabasename");
            citationsSqlTableName = (String) config.get("citations_sqltablename");

            buildReportChat = (String) config.get("build_report_chat");
            if (config.get("repeater").equals("true")) {
                repeater = true;
                repeaterChatId = (String) config.get("repeater_chat_id");
                chatsToRepeat = (String) config.get("repeater_chats_to_repeat");
                parseChatsToRepeat();
            } else repeater = false;

            if (config.get("reposter").equals("true")) {
                reposter = true;
                reposterControlChatId = (String) config.get("reposter_control_chat_id");
                chatsToRepostFrom = (String) config.get("reposter_chats_to_repost_from");
                reposterFinalDestId = (String) config.get("reposter_final_dest_chat_id");
                parseChatsToRepost();
            } else reposter = false;

            botUserNames = (String) config.get("bot_usernames");
            parseBotUsernames();
        } catch (ParseException | IOException e) {
            e.printStackTrace();
            Haruka.crash(Config.class.getSimpleName(), "Could not read config file.");
        }
    }

    private void parseBotUsernames() {
        String[] botUserNamesArr = botUserNames.split(",");
        botUserNamesList.addAll(Arrays.asList(botUserNamesArr));
    }

    private void parseChatsToRepeat() {
        String[] repeatChatsArr = chatsToRepeat.split(",");
        chatsToRepeatList.addAll(Arrays.asList(repeatChatsArr));
    }

    private void parseChatsToRepost() {
        String[] repostChatsArr = chatsToRepostFrom.split(",");
        chatsToRepostFromList.addAll(Arrays.asList(repostChatsArr));
    }

    private void checkConfigExistence() {
        File confFile = new File("config/haru.json");
        if (!confFile.exists()) {
            Haruka.crash(Config.class.getSimpleName(), "There is no config file! Exiting...");
        }
    }

    public String getRepeaterChatId() {
        return repeaterChatId;
    }

    public ArrayList<String> getChatsToRepeatList() {
        return chatsToRepeatList;
    }

    boolean isRepeater() {
        return repeater;
    }

    public String getLogSqlDatabaseName() {
        return logSqlDatabaseName;
    }

    String getBotToken() {
        return botToken;
    }

    public String getLogSqlServer() {
        return logSqlServer;
    }

    public String getLogSqlUsername() {
        return logSqlUsername;
    }

    public String getLogSqlPass() {
        return logSqlPass;
    }

    public String getReposterControlChatId() {
        return reposterControlChatId;
    }

    public String getReposterFinalDestId() {
        return reposterFinalDestId;
    }

    public ArrayList<String> getChatsToRepostFromList() {
        return chatsToRepostFromList;
    }

    public boolean isReposter() {
        return reposter;
    }

    public String getQuotesSqlServer() {
        return citationsSqlServer;
    }

    public String getQuotesSqlUsername() {
        return citationsSqlUsername;
    }

    public String getQuotesSqlPass() {
        return citationsSqlPass;
    }

    public String getQuotesSqlDatabaseName() {
        return citationsSqlDatabaseName;
    }

    public String getLogSqlTableName() {
        return logSqlTableName;
    }

    public String getCitationsSqlTableName() {
        return citationsSqlTableName;
    }

    public ArrayList<String> getBotUserNamesList() {
        return botUserNamesList;
    }

    public String getBuildReportChat() {
        return buildReportChat;
    }
}
