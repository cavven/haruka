package core;

import org.telegram.telegrambots.api.objects.Message;

import utils.deleetify.Deleetify;
import utils.quotations.Quotations;
import utils.gameservermanagement.Management;
import utils.general.Crutches;
import utils.sql.DatabaseManagement;
import utils.sql.SQLConnectionsHandler;

import java.io.IOException;
import java.sql.SQLException;

import static java.lang.System.exit;

public class CommandsHandler {
    public static void handle(Message message) throws IOException {
        String text = message.getText();
        switch (text.toLowerCase()) {
            case "/servers":
            case "/servers@lkkccc_bot":
                Management.manage(message);
                break;
            case "/haru_shutdown":
            case "/haru_shutdown@lkkccc_bot":
                exit(0);
                break;
            case "/haru_restart":
            case "/haru_restart@lkkccc_bot":
                Haruka.restart(message);
                break;
            case "/nkvdver":
            case "/nkvdver@lkkccc_bot":
                MessageSender.sendMessage(message.getChatId(), Haruka.version + "\n" + Haruka.versionName);
                break;
            case "/nkvdinfo":
            case "/nkvdinfo@lkkccc_bot":
                try {
                    DatabaseManagement.getUserStatistics(message);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "/uptime@lkkccc_bot":
            case "/uptime":
                MessageSender.sendMessage(message.getChatId(), Crutches.getUptime());
                break;
            case "/partbilet@lkkccc_bot":
            case "/partbilet":
                MessageSender.sendMessage(message.getChatId(), Crutches.getUserChatInfo(message));
                break;
            case "/sqlstat":
            case "/sqlstat@lkkccc_bot":
                MessageSender.sendMessage(message.getChatId(), Crutches.getSQLStat());
                break;
            case "/reconfigure":
            case "/reconfigure@lkkccc_bot":
                MessageSender.sendMessage(message.getChatId(), "Reloading configuration...");
                SQLConnectionsHandler.sqlFullReconnect();
                Haruka.currentConfig = new Config();
                break;
            case "/quote":
            case "/quote@lkkccc_bot":
                if(message.getReplyToMessage().hasText()) {
                    Quotations.addQuotation(message);
                } else MessageSender.sendReply(message.getChatId(), "This message cannot be quoted.",
                        message.getMessageId());
                break;
            case "/quotations":
            case "/quotations@lkkccc_bot":
                MessageSender.sendReply(message.getChatId(), Quotations.getQuotationsByUser(message),
                        message.getMessageId());
                break;
            case "/debug":
            case "/debug@lkkccc_bot":
                Haruka.toggleDebug();
                MessageSender.sendReply(message.getChatId(), "Debug mode toggled.", message.getMessageId());
                break;
            case "/del33tify":
                if(!Deleetify.isEnabled()) {
                    MessageSender.sendReply(message.getChatId(), "Deleetifier is turned off.",
                            message.getMessageId());
                } else {
                    String deleetified = Deleetify.deleetify(message.getReplyToMessage().getText());
                    if("".equals(deleetified)){
                    MessageSender.sendReply(message.getChatId(), "This text is not deleetifieable",
                            message.getMessageId());
                    } else {
                        MessageSender.sendReply(message.getChatId(), "Deleetified text:\n" + deleetified,
                                message.getMessageId());
                    }
                }
            default:
                break;
        }
    }

}
