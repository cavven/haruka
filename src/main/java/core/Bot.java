package core;

import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import utils.sql.DatabaseManagement;
import utils.inlines.Inlines;

import java.io.IOException;


public class Bot extends TelegramLongPollingBot {
	
	Bot(String token) {
	    botToken = token;
	}
	
    public String botToken;

    @Override
    public String getBotUsername() {
        return "LkkCcc_bot";
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasCallbackQuery()) {
            Inlines.parseInline(update.getCallbackQuery());
        }
        if (update.hasMessage()) {
            try {
                DatabaseManagement.addMessageByUpdate(update);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Haruka.messageOutput(update.getMessage());
        }
        if (update.hasChannelPost()) {
            Haruka.messageOutput(update.getChannelPost());
        }
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}

