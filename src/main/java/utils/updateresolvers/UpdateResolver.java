package utils.updateresolvers;

import org.telegram.telegrambots.api.objects.*;
import org.telegram.telegrambots.api.objects.stickers.Sticker;
import utils.general.Crutches;

import java.text.SimpleDateFormat;
import java.util.List;

public class UpdateResolver {
    public static String resolveMessages(Message message, boolean withtext, boolean withuser) {
        long timeRaw = message.getDate();
        timeRaw *= 1000;
        String text = "";
        if (withtext) {
            if (withuser && !message.isChannelMessage()) {
                SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd/MM/yyyy' 'HH:mm:ss");
                text = dateTimeFormatter.format(timeRaw) + "|" + Crutches.tgGetChat(message) +
                        "(" + message.getChatId() + ")" + "->" + Crutches.tgGetUser(message) + ": ";
            }
            if (message.hasText()) {
                return text + resolveTextMessage(message);
            }
        }
        if (message.getSticker() != null) {
            return text + resolveStickers(message.getSticker());
        }
        if (message.hasPhoto()) {
            return text + resolvePhotos(message.getPhoto(), message.getCaption());
        }
        if (message.hasDocument()) {
            return text + resolveDocuments(message.getDocument(), message.getCaption());
        }
        if (message.getAudio() != null) {
            return text + resolveAudios(message.getAudio(), message.getCaption());
        }
        if (message.getVideo() != null) {
            return text + resolveVideo(message.getVideo());
        }
        if (message.getDeleteChatPhoto() != null) {
            if (message.getDeleteChatPhoto()) {
                return text + "Chat photo deleted";
            }
        }
        if (message.getNewChatMembers() != null) {
            return text + resolveNewUsers(message.getNewChatMembers());
        }
        if (message.getNewChatPhoto() != null) {
            return text + "Chat photo changed:" + resolvePhotos(message.getNewChatPhoto(), null);
        }
        if (message.getNewChatTitle() != null) {
            return text + "Chat title changed to \"" + message.getNewChatTitle() + "\"";
        }
        if (message.getChannelChatCreated() != null) {
            return text + "Channel created";
        }
        if (message.getGroupchatCreated() != null) {
            if (message.getGroupchatCreated()) {
                return text + "Chat created";
            }
        }
        if (message.getSuperGroupCreated() != null) {
            if (message.getSuperGroupCreated()) {
                return text + "Supergroup created";
            }
        }
        if (message.getMigrateToChatId() != null) {
            return text + "The chat has become a supergroup with this id: " + message.getMigrateToChatId();
        }
        return null;
    }

    private static String resolveTextMessage(Message message) {
        return message.getText();
    }

    private static String resolvePhotos(List<PhotoSize> photos, String caption) {
        StringBuilder resolvedText;
        resolvedText = new StringBuilder(photos.size() + " photos total:\n");
        for (int i = 0; i < photos.size(); i++) {
            resolvedText.append("Photo №").append(i).append(": Id:").append(photos.get(i).getFileId()).append("\n");
            resolvedText.append("Resolution: ").append(photos.get(i).getWidth()).append("x").append(photos.get(i).getHeight()).append("\n");
            resolvedText.append("Size: ").append(photos.get(i).getFileSize() / 1000).append("kb").append("\n");
        }
        if (caption != null)
            resolvedText.append("Caption: ").append(caption);
        return resolvedText.toString();
    }

    private static String resolveDocuments(Document document, String caption) {
        String resolvedText;
        resolvedText = "Document: " + document.getFileName() + "\n";
        resolvedText += "Id: " + document.getFileId() + "\n";
        resolvedText += "Size: " + document.getFileSize() / 1000 + "kb" + "\n";
        resolvedText += "Mimetype: " + document.getMimeType() + "\n";
        if (document.getThumb() != null) {
            resolvedText += resolveThumbnails(document.getThumb());
        }
        if (caption != null)
            resolvedText += "Caption: " + caption;
        return resolvedText;
    }

    private static String resolveAudios(Audio audio, String caption) {
        String resolvedText;
        resolvedText = "Audio file: " + audio.getPerformer() + " - " + audio.getTitle() + "\n";
        resolvedText += "Id: " + audio.getFileId() + "\n";
        resolvedText += "Size: " + audio.getFileSize() / 1000 + "kb" + "\n";
        resolvedText += "Duration: " + Crutches.secsToTime(audio.getDuration()) + "\n";
        resolvedText += "Mimetype: " + audio.getMimeType() + "\n";
        if (caption != null)
            resolvedText += "Caption: " + caption;
        return resolvedText;
    }

    private static String resolveStickers(Sticker sticker) {
        String resolvedText;
        resolvedText = "Sticker: " + sticker.getEmoji() + "\n";
        resolvedText += "Id: " + sticker.getFileId() + "\n";
        resolvedText += "Size: " + sticker.getFileSize() / 1000 + "kb" + "\n";
        resolvedText += "Set name: " + sticker.getSetName() + "\n";
        resolvedText += "Mask position: " + sticker.getMaskPosition() + "\n";
        resolvedText += "Resolution: " + sticker.getHeight() + "x" + sticker.getWidth() + "\n";
        if (sticker.getThumb() != null) {
            resolvedText += resolveThumbnails(sticker.getThumb());
        }
        return resolvedText;
    }

    private static String resolveThumbnails(PhotoSize thumbnail) {
        String resolvedText = "Thumbnail: \n";
        resolvedText += "   Id: " + thumbnail.getFileId() + "\n";
        resolvedText += "   Resolution: " + thumbnail.getWidth() + "x" + thumbnail.getHeight() + "\n";
        resolvedText += "   Size: " + thumbnail.getFileSize() / 1000 + "kb" + "\n";
        return resolvedText;
    }

    private static String resolveNewUsers(List<User> userList) {
        StringBuilder resolvedText = new StringBuilder("New users added:\n");
        for (int i = 0; i < userList.size(); i++) {
            resolvedText.append(i).append(". @").append(userList.get(i).getUserName()).append(":\nName: ");
            if (userList.get(i).getFirstName() != null) {
                resolvedText.append(userList.get(i).getFirstName()).append(" ");
            }
            if (userList.get(i).getLastName() != null) {
                resolvedText.append(userList.get(i).getLastName()).append(" ");
            }
            resolvedText.append("\nId:").append(userList.get(i).getId()).append("\n");
            resolvedText.append("Bot?:").append(userList.get(i).getBot());
        }
        return resolvedText.toString();
    }

    private static String resolveVideo(Video video) {
        String resolvedText;
        resolvedText = "Video: ";
        resolvedText += "Id: " + video.getFileId() + "\n";
        resolvedText += "Size: " + video.getFileSize() / 1000 + "kb" + "\n";
        resolvedText += "Resolution: " + video.getWidth() + "x" + video.getHeight() + "\n";
        resolvedText += "Duration: " + Crutches.secsToTime(video.getDuration()) + "\n";
        resolvedText += "Mimetype: " + video.getMimeType() + "\n";
        if (video.getThumb() != null) {
            resolvedText += resolveThumbnails(video.getThumb());
        }
        return resolvedText;
    }
}
