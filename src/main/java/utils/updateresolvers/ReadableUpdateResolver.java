package utils.updateresolvers;

import core.CommandsHandler;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.User;
import utils.general.Crutches;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class ReadableUpdateResolver {
    public static String resolveMessages(Message message, boolean withtext, boolean withuser) throws IOException {
        long timeRaw = message.getDate();
        timeRaw *= 1000;
        SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("dd/MM/yyyy' 'HH:mm:ss");
        String text = "";
        if (withtext) {
            if (withuser) {
                text = dateTimeFormatter.format(timeRaw) + "|" + message.getChat().getTitle() +
                        "(" + message.getChatId() + ")" + "->" + Crutches.tgGetUser(message) + ": ";
            }
            if (message.hasText()) {
                if (message.getText().toCharArray()[0] == '/') {
                    CommandsHandler.handle(message);
                    return text + "Has sent a command: " + message.getText();
                }
                return text + resolveTextMessage(message);
            }
        }
        if (message.getCaption() != null) {
            text += message.getCaption() + "\n";
        }
        if (message.getSticker() != null) {
            return text + "<Has sent a sticker>";
        }
        if (message.hasPhoto()) {
            return text + "<Has sent a photo/photos>";
        }
        if (message.hasDocument()) {
            return text + "<Has sent a document>";
        }
        if (message.getAudio() != null) {
            return text + "<Has sent an audio file>";
        }
        if (message.getVideo() != null) {
            return text + "<Has sent a video>";
        }
        if (message.getDeleteChatPhoto()) {
            return text + "Chat photo deleted";
        }
        if (message.getNewChatMembers() != null) {
            return text + resolveNewUsers(message.getNewChatMembers());
        }
        if (message.getNewChatPhoto() != null) {
            return text + "<Chat photo changed>";
        }
        if (message.getNewChatTitle() != null) {
            return text + "Chat title changed to \"" + message.getNewChatTitle() + "\"";
        }
        if (message.getChannelChatCreated() != null) {
            return text + "Channel created";
        }
        if (message.getGroupchatCreated()) {
            return text + "Chat created";
        }
        if (message.getSuperGroupCreated()) {
            return text + "Supergroup created";
        }
        if (message.getMigrateToChatId() != null) {
            return text + "The chat has become a supergroup with this id: " + message.getMigrateToChatId();
        }
        return null;
    }

    private static String resolveTextMessage(Message message) {
        return message.getText();
    }

    private static String resolveNewUsers(List<User> userList) {
        StringBuilder resolvedText = new StringBuilder("New users added:\n");
        for (int i = 0; i < userList.size(); i++) {
            resolvedText.append(i).append(". @").append(userList.get(i).getUserName()).append(":\nName: ");
            if (userList.get(i).getFirstName() != null) {
                resolvedText.append(userList.get(i).getFirstName()).append(" ");
            }
            if (userList.get(i).getLastName() != null) {
                resolvedText.append(userList.get(i).getLastName()).append(" ");
            }
            resolvedText.append("\nId:").append(userList.get(i).getId()).append("\n");
            resolvedText.append("Bot?:").append(userList.get(i).getBot());
        }
        return resolvedText.toString();
    }
}
