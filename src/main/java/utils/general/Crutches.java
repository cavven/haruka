package utils.general;

import core.Haruka;
import core.MessageSender;
import org.telegram.telegrambots.api.objects.Message;
import utils.sql.SQLConnectionsHandler;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class Crutches {

    public static String getUserChatInfo(Message message) {
		String outp = "====Партбилет====\n";
		outp+="Гражданин: "+ tgGetUser(message)+"\n";
		outp+="Код отделения партии: "+message.getChatId()+"\n";
		outp+="Номер партбилета: "+message.getFrom().getId();
		return outp;
	}

    public static void buildReport(){
       String buildReport = readBuildInfo();
        if(buildReport!=null){
            MessageSender.sendMessage(Long.parseLong(Haruka.currentConfig.getBuildReportChat()), buildReport);
            File repFileToDel = new File("jks_buildinfo");
            repFileToDel.delete();
        }
    }

    private static String readBuildInfo(){
	    if(new File("jks_buildinfo").exists()){
            try {
                return new Scanner(new File("jks_buildinfo")).useDelimiter("\\Z").next();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static String getSQLStat() {
        StringBuilder Message = new StringBuilder();
        String query;
        try {
            Statement stm = SQLConnectionsHandler.getLogDBConnection().createStatement();
            ResultSet rs;
            query = "SELECT count(DISTINCT user_id) FROM aka_log";
            rs = stm.executeQuery(query);
            while (rs.next()) {
                Message.append(rs.getString("count(DISTINCT user_id)"));
                Message.append(" участников в ");
            }
            query = "SELECT count(DISTINCT chat_id) FROM aka_log WHERE chat_id=user_id";
            rs = stm.executeQuery(query);
            while (rs.next()) {
                Message.append(rs.getString("count(DISTINCT chat_id)"));
                Message.append(" личках и ");
            }
            query = "SELECT count(DISTINCT chat_id) FROM aka_log WHERE chat_id!=user_id";
            rs = stm.executeQuery(query);
            while (rs.next()) {
                Message.append(rs.getString("count(DISTINCT chat_id)"));
                Message.append(" чатах.\n");
            }
            query = "SELECT count(*) FROM aka_log";
            rs = stm.executeQuery(query);
            while (rs.next()) {
                Message.append(rs.getString("count(*)"));
                Message.append(" сообщений всего в базе.\n");
            }
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Haruka.output(Message.toString());
        return (Message.toString().trim());
    }

    public static String getUptime() {
        String Message;
        long uptime = System.currentTimeMillis() - Haruka.getStartTime();
        int secs = 0, mins = 0, hours = 0, days = 0;
        while (uptime >= 1000) {
            secs++;
            uptime -= 1000;
            if (secs == 60) {
                mins++;
                secs = 0;
            }
            if (mins == 60) {
                hours++;
                mins = 0;
            }
            if (hours == 24) {
                days++;
                hours = 0;
            }
        }
        Message = "";
        Message += "Uptime: ";
        if (days != 0) {
            Message += days + " days ";
        }
        if (hours != 0) {
            Message += hours + " hours ";
        }
        if (mins != 0) {
            Message += mins + " minutes ";
        }
        Message += secs + " seconds ";
        return Message;
    }

    public static void checkAndCreateDirectories(String path) {
        File directory = new File(path);
        if (!directory.exists()) {
            directory.mkdirs();
        }
    }

    public static String secsToTime(int inpSecs) {
        long uptime = inpSecs * 1000L;
        int secs = 0, mins = 0, hours = 0, days = 0;
        while (uptime >= 1000) {
            secs++;
            uptime -= 1000;
            if (secs == 60) {
                mins++;
                secs = 0;
            }
            if (mins == 60) {
                hours++;
                mins = 0;
            }
            if (hours == 24) {
                days++;
                hours = 0;
            }
        }
        String Message = "";
        if (days != 0) {
            Message += days + " days ";
        }
        if (hours != 0) {
            Message += hours + " hours ";
        }
        if (mins != 0) {
            Message += mins + " minutes ";
        }
        Message += secs + " seconds ";
        return Message;
    }

    public static String tgGetUser(Message message) {
        String reply = "";
        if (message.getFrom() != null) {
            if (message.getFrom().getFirstName() != null) {
                reply += message.getFrom().getFirstName();
            }
            if (message.getFrom().getLastName() != null) {
                reply += " " + message.getFrom().getLastName();
            }
            if (message.getFrom().getUserName() != null) {
                reply += " (@" + message.getFrom().getUserName() + ")";
            }
            return reply;
        }
        return "UNKNOWN USER";
    }

    public static String tgGetChat(Message message) {
        String reply = "";
        if (message.getChat().getFirstName() != null) {
            reply += message.getFrom().getFirstName();
        }
        if (message.getChat().getLastName() != null) {
            reply += " " + message.getFrom().getLastName();
        }
        if (message.getChat().getTitle() != null) {
            reply += " " + message.getChat().getTitle();
        }
        if (message.getChat().getUserName() != null) {
            reply += " (@" + message.getChat().getUserName() + ")";
        }
        return reply;
    }

    public static String tgGetChatType(Message message) {
        if (message.getChatId().equals(Long.parseLong((message.getFrom().getId().toString())))) return "private";
        if (message.getChat().getAllMembersAreAdministrators() != null) {
            if (message.getChat().getAllMembersAreAdministrators()) {
                return "group";
            } else return "supergroup";
        } else return "supergroup";
    }

    public static String tgGetUserWithoutUsername(Message message) {
        String reply = "";
        if (message.getFrom().getFirstName() != null) {
            reply += message.getFrom().getFirstName();
        }
        if (message.getFrom().getLastName() != null) {
            reply += " " + message.getFrom().getLastName();
        }
        if("".equals(reply)) {
            return "UNKNOWN USER";
        } else return reply;
    }

}
