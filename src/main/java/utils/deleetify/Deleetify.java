package utils.deleetify;

import core.Haruka;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Deleetify {
    private static boolean enabled = false;
    private static JSONObject deleetifierJson = null;
    public static void init(){
        try{
            JSONParser parser = new JSONParser();
            deleetifierJson = (JSONObject) parser.parse(new FileReader("config/deleetify.json"));
            enabled = true;
            Haruka.output("Deleetifyer is initialized.");
        } catch (ParseException e){
            Haruka.output("Could not parse deleetify config file. Deleetifying is turned off.");
        } catch (FileNotFoundException e){
            Haruka.output("There is no deleetify config file. Deleetifying is turned off.");
        } catch (IOException e){
            Haruka.output("Could not read deleetify config file. Deleetifying is turned off.");
        }
    }

    public static String deleetify(String text) {
        char[] textArr = text.toCharArray();
        StringBuilder bld = new StringBuilder();
        String res;
        for (char aChar : textArr) {
            if (Character.isUpperCase(aChar)) {
                res = ((String) deleetifierJson.get(String.valueOf(aChar).toLowerCase())).toUpperCase();
                if (res == null) {
                    bld.append(aChar);
                    continue;
                }
            } else {
                res = ((String) deleetifierJson.get(String.valueOf(aChar)));
                if (res == null) {
                    bld.append(aChar);
                    continue;
                }
            }
            bld.append(res);
        }
        return bld.toString();
    }

    public static boolean isEnabled() {
        return enabled;
    }
}
