package utils.sql;

import core.Haruka;

import java.lang.reflect.Method;
import java.sql.Connection;

public class SQLConnectionsHandler {
    private static Connection logDBConnection = null;
    private static Connection citationsDBConnection = null;
    public static synchronized Connection getLogDBConnection(){
        if(logDBConnection==null){
            Haruka.output("Connecting to log database...");
            logDBConnection = createLogDBConnection();
            Haruka.output("Connected to log database!");
        }
        return logDBConnection;
    }

    static synchronized Connection getCitationsDBConnection(){
        if(citationsDBConnection==null){
            Haruka.output("Connecting to quotations database...");
            citationsDBConnection = createCitationsDBConnection();
            Haruka.output("Connected to quotations database!");
        }
        return citationsDBConnection;
    }

    static void reconnectLogDB(){
        logDBConnection = null;
    }

    static void reconnectCitationsDB(){
        citationsDBConnection = null;
    }

    public static void sqlFullReconnect(){
        reconnectLogDB();
        reconnectCitationsDB();
    }

    private static Connection createLogDBConnection(){
        return new SQLConnector(Haruka.currentConfig.getLogSqlServer(),
                Haruka.currentConfig.getLogSqlUsername(),
                Haruka.currentConfig.getLogSqlPass(),
                Haruka.currentConfig.getLogSqlDatabaseName()).connection;
    }

    private static Connection createCitationsDBConnection(){
        return new SQLConnector(Haruka.currentConfig.getQuotesSqlServer(),
                Haruka.currentConfig.getQuotesSqlUsername(),
                Haruka.currentConfig.getQuotesSqlPass(),
                Haruka.currentConfig.getQuotesSqlDatabaseName()).connection;
    }
}
