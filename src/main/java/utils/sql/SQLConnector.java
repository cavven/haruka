package utils.sql;

import com.mysql.cj.jdbc.MysqlDataSource;
import core.Haruka;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

class SQLConnector {
    Connection connection = null;
    SQLConnector(String url, String user, String password, String db_name) {
        try {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser(user);
        dataSource.setPassword(password);
        dataSource.setServerName(url);
        dataSource.setDatabaseName(db_name);
        dataSource.setAutoReconnect(true);
        dataSource.setAutoReconnectForPools(true);
        dataSource.setReconnectAtTxEnd(true);
        dataSource.setCharacterEncoding("utf8");
        dataSource.setUseSSL(false);
        connection = dataSource.getConnection();
        Statement collationStatement = connection.createStatement();
        collationStatement.executeQuery("SET NAMES utf8mb4");
        } catch (SQLException e) {
            e.printStackTrace();
            Haruka.crash(this.getClass().getSimpleName(), "Could not establish " +
                    "SQL connection to database.\n" +
                    "Address: "+url+"\n" +
                    "Database: "+db_name+"\n" +
                    "User: "+user);
        }
    }
}
