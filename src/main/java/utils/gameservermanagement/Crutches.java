package utils.gameservermanagement;

import core.Haruka;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Crutches {
    static ArrayList<String> getMapsFromMapFile(String gameName) throws IOException {
        utils.general.Crutches.checkAndCreateDirectories(new File(".").getAbsolutePath() + "/config/servermaps/");
        File mapsFile = new File(new File(".").getAbsolutePath() + "/config/servermaps/" + gameName);
        Haruka.output("Reading maps for " + gameName + " from " + mapsFile.getAbsolutePath());
        try (BufferedReader reader = new BufferedReader(new FileReader(mapsFile))){
            ArrayList<String> mapList = new ArrayList<>();
            String currMap;
            while ((currMap = reader.readLine()) != null) {
                mapList.add(currMap);
            }
            return mapList;
        } catch (java.io.FileNotFoundException e) {
            Haruka.output("There is no map file for " + gameName + "!");
        }
        return new ArrayList<>();
    }

    static void serverManagementOutput(String text) {
        Haruka.output("Server management: " + text);
    }
}
