package utils.repeater;

import java.util.ArrayList;

import org.telegram.telegrambots.api.methods.ForwardMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import core.Haruka;

public class Repeater {
	private static ArrayList<String> chatsToRepeat = Haruka.currentConfig.getChatsToRepeatList();
	private static String repeaterChatId = Haruka.currentConfig.getRepeaterChatId();
	public static synchronized void checkAndRepeat(Message message) {
	if(chatsToRepeat.contains(String.valueOf(message.getChatId()))){
		repeat(message);
	}
	}
	private static void repeat(Message message) {
        try {
		ForwardMessage frwMsg = new ForwardMessage();
		frwMsg.setMessageId(message.getMessageId());
		frwMsg.setChatId(repeaterChatId);
		frwMsg.setFromChatId(message.getChatId());
		DefaultBotOptions options = new DefaultBotOptions();
        DefaultAbsSender sender = new DefaultAbsSender(options) {
            @Override
            public String getBotToken() {
                return Haruka.HarukaBot.getBotToken();
            }
        };
        	sender.execute(frwMsg);
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
	}
}
