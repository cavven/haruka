package utils.inlines;

import core.Haruka;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import org.telegram.telegrambots.api.methods.send.SendPhoto;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import utils.gameservermanagement.Game;
import static utils.gameservermanagement.Management.getGameByName;
import utils.reposter.Reposter;

@SuppressWarnings("unused")
public class Inlines {
    private static InlineKeyboardMarkup createInlines(String Label, ArrayList<String> entries) {
        InlineKeyboardMarkup inlineMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> buttonList = new ArrayList<>();
        for (String entry : entries) {
            buttonList.add(createInlineButton(Label, entry));
        }
        List<List<InlineKeyboardButton>> buttons = splitButtonList(buttonList);
        inlineMarkup.setKeyboard(buttons);
        return inlineMarkup;
    }
    
    public static void parseInline(CallbackQuery query) {
        String inline = query.getData();
        inlineOutput("Got inline query: " + query.getData());
        String[] inlineArr = inline.split(" ");
        Game inlineGame;
        if (inline.contains("management")) {
            if ((inlineGame = getGameByName(inlineArr[1])) != null) {
                inlineGame.parseCommand(query.getData(), query.getMessage().getChatId(), query.getMessage().getMessageId());
            }
        } else {
            if ((inlineGame = getGameByName(inlineArr[0])) != null) {
                inlineGame.parseCommand(query.getData(), query.getMessage().getChatId(), query.getMessage().getMessageId());
            }
        if(inline.contains("❌")||inline.contains("✔")){
                if(Haruka.currentConfig.isReposter()){
                Reposter.parseCommand(inline, query.getMessage().getChatId(), query.getMessage().getMessageId());
                }
        }
    }
    }
        
    private static void inlineOutput(String text){
        Haruka.output("Inlines: " + text);
    }
    
    private static InlineKeyboardButton createInlineButton(String label, String name) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setCallbackData(label + " " + name);
        button.setText(name);
        return button;
    }

    private static List<List<InlineKeyboardButton>> splitButtonList(List<InlineKeyboardButton> list) {
        List<List<InlineKeyboardButton>> listOfLists = new ArrayList<>();
        for (int i = 0; i < list.size() / 4 + 1; i++) {
            listOfLists.add(new ArrayList<>());
        }
        for (int i = 0; i < list.size(); i++) {
            listOfLists.get(i / 4).add(list.get(i));
        }
        return listOfLists;
    }

    public static void createAndSendInlines(long chatId, ArrayList<String> buttons, String Label) throws TelegramApiException {
        InlineKeyboardMarkup keyboardMarkup = createInlines(Label, buttons);
        DefaultBotOptions opts = new DefaultBotOptions();
        DefaultAbsSender sender = new DefaultAbsSender(opts) {
            @Override
            public String getBotToken() {
                return Haruka.HarukaBot.botToken;
            }
        };
        SendMessage sendMessage = new SendMessage();
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setChatId(chatId);
        sendMessage.setText(Label);
        sender.execute(sendMessage);
    }

    public static void createAndEditInlines(long chatId, ArrayList<String> buttons, String label, int msgId) throws TelegramApiException {
        if (!"management".equals(label) && !buttons.contains("back"))
            buttons.add("back");
        InlineKeyboardMarkup keyboardMarkup = createInlines(label, buttons);
        DefaultBotOptions opts = new DefaultBotOptions();
        DefaultAbsSender sender = new DefaultAbsSender(opts) {
            @Override
            public String getBotToken() {
                return Haruka.HarukaBot.botToken;
            }
        };
        EditMessageText editText = new EditMessageText();
        editText.setText(label);
        editText.setChatId(chatId);
        editText.setMessageId(msgId);
        sender.execute(editText);
        EditMessageReplyMarkup editReplyMarkup = new EditMessageReplyMarkup();
        editReplyMarkup.setReplyMarkup(keyboardMarkup);
        editReplyMarkup.setChatId(chatId);
        editReplyMarkup.setMessageId(msgId);
        sender.execute(editReplyMarkup);
    }

    private static SendMessage attachInlinesToMessage(String label, Message message, ArrayList<String> entries){
        InlineKeyboardMarkup inlineMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> buttonList = new ArrayList<>();
        for (String entry : entries) {
            buttonList.add(createInlineButton(label, entry));
        }
        List<List<InlineKeyboardButton>> buttons = splitButtonList(buttonList);
        inlineMarkup.setKeyboard(buttons);
        SendMessage messageWithInlines = new SendMessage();
        messageWithInlines.setReplyMarkup(inlineMarkup);
        messageWithInlines.setText(message.getText());
        return messageWithInlines;
    }
        public static SendMessage attachInlinesToMessage(String label, SendMessage sendMessage, ArrayList<String> entries){
        InlineKeyboardMarkup inlineMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> buttonList = new ArrayList<>();
        for (String entry : entries) {
            buttonList.add(createInlineButton(label, entry));
        }
        List<List<InlineKeyboardButton>> buttons = splitButtonList(buttonList);
        inlineMarkup.setKeyboard(buttons);
        sendMessage.setReplyMarkup(inlineMarkup);
        return sendMessage;
    }
        public static SendPhoto attachInlinesToPhotoMessage(String label, SendPhoto sendPhoto, ArrayList<String> entries){
        InlineKeyboardMarkup inlineMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> buttonList = new ArrayList<>();
        for (String entry : entries) {
            buttonList.add(createInlineButton(label, entry));
        }
        List<List<InlineKeyboardButton>> buttons = splitButtonList(buttonList);
        inlineMarkup.setKeyboard(buttons);
        sendPhoto.setReplyMarkup(inlineMarkup);
        return sendPhoto;
    }
        public static SendPhoto attachInlinesToPhotoMessage(String label, Message message, ArrayList<String> entries, String photo){
        InlineKeyboardMarkup inlineMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> buttonList = new ArrayList<>();
        for (String entry : entries) {
            buttonList.add(createInlineButton(label, entry));
        }
        List<List<InlineKeyboardButton>> buttons = splitButtonList(buttonList);
        inlineMarkup.setKeyboard(buttons);
        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setPhoto(photo);
        sendPhoto.setReplyMarkup(inlineMarkup);
        return sendPhoto;
    }
}
